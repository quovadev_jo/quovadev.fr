<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::connection('mongodb')->collection('users')->truncate();
        DB::connection('mongodb')->collection('users')->insert(
        	[
        		[
        			'name' => 'PowerAdmin',
        			'email' => 'poweradmin@toto.com',
        			'password' => bcrypt('PowerAdmin#1111'),
        			'status' => 'admin'
        		]
        	]
        );
    }
}

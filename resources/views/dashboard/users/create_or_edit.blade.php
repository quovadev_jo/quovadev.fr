@extends('dashboard._layout')

@section('content')
	<ol class="breadcrumb bg-white">
		<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="#">Users</a></li>
		<li class="breadcrumb-item"></li>
	</ol>
	
	<div class="card">
		<div class="card-body">
			<form action="{{ $action }}" method="POST">
				@csrf
				@if(str_contains(Route::currentRouteAction(), 'edit'))
					@method('put')
				@endif
				<div class="form-group">
					<div class="row">
						<div class="col-md-1">
							<label for="name">Name</label>
						</div>
						<div class="col-md-6">
							<input type="text" class="form-control" name="name" required value="{{ $user->name ?? '' }}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-1">
							<label for="email">Email</label>
						</div>
						<div class="col-md-6">
							<input type="email" class="form-control" name="email" required value="{{ $user->email ?? '' }}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-1">
							<label for="password">Password</label>
						</div>
						<div class="col-md-6">
							<input type="text" class="form-control" name="password">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6 offset-md-1">
							<button type="submit" class="btn btn-success btn-lg btn-block">Send</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
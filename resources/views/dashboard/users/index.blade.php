@extends('dashboard._layout')

@section('content')
	<ol class="breadcrumb bg-white">
		<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
		<li class="breadcrumb-item">Users</li>
	</ol>
	
	<div class="card">
		<div class="card-body">
			<div class="mb-3 text-right">
				<a href="{{ action('Dashboard\UsersController@create') }}" class="btn btn-primary btn-lg">Create user <i class="fas fa-plus"></i></a>
			</div>

			<table class="table table-stripped">
				<thead>
					<th>Name</th>
					<th>Email</th>
					<th>Created at</th>
					<th>Updated at</th>
					<th>Delete</th>
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
							<td><a href="{{ action('Dashboard\UsersController@edit', $user->id) }}">{{ $user->name ?? '' }}</a></td>
							<td><a href="{{ action('Dashboard\UsersController@edit', $user->id) }}">{{ $user->email ?? '' }}</a></td>
							<td>{{ $user->created_at ?? '' }}</td>
							<td>{{ $user->updated_at ?? '' }}</td>
							<td>
								<form action="{{ action('Dashboard\UsersController@destroy', $user->id) }}" method="POST">
									@csrf
									@method('delete')
									<button type="submit" class="btn btn-danger btn-sm">Delete <i class="fas fa-trash-alt"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
@extends('dashboard._layout')

@section('content')
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
    		<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  		</div>
  		<div class="row">
  			<div class="col-md">
				<div class="card border-left-primary shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Users</div>
								<div class="h5 mb-0 font-weight-bold text-gray-800">1</div>
							</div>
							<div class="col-auto">
								<i class="fas fa-users fa-2x text-gray-300"></i>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="#" class="btn btn-primary btn-lg btn-block">See all users</a>
					</div>
				</div>
			</div>
			<div class="col-md">
				<div class="card border-left-primary shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Articles</div>
								<div class="h5 mb-0 font-weight-bold text-gray-800">1</div>
							</div>
							<div class="col-auto">
								<i class="fas fa-scroll fa-2x text-gray-300"></i>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="#" class="btn btn-primary btn-lg btn-block">See all articles</a>
					</div>
				</div>
			</div>
			<div class="col-md">
				<div class="card border-left-primary shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jobs</div>
								<div class="h5 mb-0 font-weight-bold text-gray-800">1</div>
							</div>
							<div class="col-auto">
								<i class="fas fa-briefcase fa-2x text-gray-300"></i>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="#" class="btn btn-primary btn-lg btn-block">See all jobs</a>
					</div>
				</div>
			</div>
  		</div>
	</div>
	
@endsection
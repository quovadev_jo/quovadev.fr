@extends('dashboard._layout')

@push('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.11.2/simplemde.min.css">
@endpush

@section('content')
	<ol class="breadcrumb bg-white">
		<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="#">Jobs</a></li>
		<li class="breadcrumb-item"></li>
	</ol>
	
	<div class="card">
		<div class="card-body">
			<form action="{{ $action }}" method="POST" enctype="multipart/form-data">
				@csrf
				@if(str_contains(Route::currentRouteAction(), 'edit'))
					@method('put')
				@endif
				<div class="form-group">
					<div class="row">
						<div class="col-md-1">
							<label for="title">Title</label>
						</div>
						<div class="col-md-11">
							<input type="text" class="form-control" name="title" required value="{{ $job->title ?? old('title') ?? '' }}">
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-1">
							<label for="content">Content</label>
						</div>
						<div class="col-md">
							<textarea id="text_content" name="content" class="text-control" rows="10">{{ $job->content ?? old('content') ?? '' }}</textarea>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-md offset-md-1">
							<button type="submit" class="btn btn-success btn-lg btn-block">Send</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@push('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.11.2/simplemde.min.js"></script>
	<script>
		const content = new SimpleMDE({
			element: document.querySelector('#text_content')
		});
	</script>
@endpush
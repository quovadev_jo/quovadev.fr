@extends('dashboard._layout')

@section('content')
	<ol class="breadcrumb bg-white">
		<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
		<li class="breadcrumb-item">Jobs</li>
	</ol>
	
	<div class="card">
		<div class="card-body">
			<div class="mb-3 text-right">
				<a href="{{ action('Dashboard\JobsController@create') }}" class="btn btn-primary btn-lg">Create job <i class="fas fa-plus"></i></a>
			</div>

			<table class="table table-stripped">
				<thead>
					<th>Title</th>
					<th>Content</th>
					<th>Created at</th>
					<th>Updated at</th>
					<th>Delete</th>
				</thead>
				<tbody>
					@foreach($jobs as $job)
						<tr>
							<td><a href="{{ action('Dashboard\JobsController@edit', $job->id) }}">{{ $job->title ?? '' }}</a></td>
							<td><a href="{{ action('Dashboard\JobsController@edit', $job->id) }}">{{ $job->content ?? '' }}</a></td>
							<td>{{ $job->created_at ?? '' }}</td>
							<td>{{ $job->updated_at ?? '' }}</td>
							<td>
								<form action="{{ action('Dashboard\JobsController@destroy', $job->id) }}" method="POST">
									@csrf
									@method('delete')
									<button type="submit" class="btn btn-danger btn-sm">Delete <i class="fas fa-trash-alt"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
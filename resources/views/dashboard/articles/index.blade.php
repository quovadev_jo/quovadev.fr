@extends('dashboard._layout')

@section('content')
	<ol class="breadcrumb bg-white">
		<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
		<li class="breadcrumb-item">Articles</li>
	</ol>
	
	<div class="card">
		<div class="card-body">
			<div class="mb-3 text-right">
				<a href="{{ action('Dashboard\ArticlesController@create') }}" class="btn btn-primary btn-lg">Create article <i class="fas fa-plus"></i></a>
			</div>

			<table class="table table-stripped">
				<thead>
					<th>Title</th>
					<th>Preview</th>
					<th>Created at</th>
					<th>Updated at</th>
					<th>Delete</th>
				</thead>
				<tbody>
					@foreach($articles as $article)
						<tr>
							<td><a href="{{ action('Dashboard\ArticlesController@edit', $article->id) }}">{{ $article->title ?? '' }}</a></td>
							<td><a href="{{ action('Dashboard\ArticlesController@edit', $article->id) }}">{{ $article->preview ?? '' }}</a></td>
							<td>{{ $article->created_at ?? '' }}</td>
							<td>{{ $article->updated_at ?? '' }}</td>
							<td>
								<form action="{{ action('Dashboard\ArticlesController@destroy', $article->id) }}" method="POST">
									@csrf
									@method('delete')
									<button type="submit" class="btn btn-danger btn-sm">Delete <i class="fas fa-trash-alt"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
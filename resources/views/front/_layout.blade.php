<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-12/css/all.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
		<link rel="stylesheet" href="/css/app.css">
		<title>quovadev.fr</title>
		@stack('css')
	</head>
	<body>
		@php
			$nav = [
				[
					'title' => 'Expertises',
					'action' => '/#skills',
					
				],
				[
					'title' => 'Réferences',
					'action' => '/#refs',
					
				],
				[
					'title' => 'Nos offres',
					'action' => '/#offres',
					
				],
				[
					'title' => 'Infos',
					'action' => '/#infos',
					
				],
				[
					'title' => 'Contact',
					'action' => '/#contact',
					
				],
				[
					'title' => 'Blog',
					'action' => '/blog'
				]
			];
		@endphp
		<header>
			<nav class="nav" id="nav">
				<div id="logo">
					<a href="/"><img src="/images/logo.png" alt="logo"></a>
				</div>
				<ul class="nav-ul">
					@foreach($nav as $link)
						<li><a data-scroll href="{{ $link['action'] }}">{{ $link['title'] }}</a></li>
					@endforeach
				</ul>
				<div class="menu-button">
					<i class="fas fa-bars"></i>
				</div>
			</nav>
		</header>
		
		<main>
			@yield('content')
		</main>

		<footer>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md">
						<div class="title">Contact</div>
						<div class="mb-3 pr-5">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis error, recusandae itaque, vel consequuntur accusamus culpa. Eos aliquid incidunt, eaque reiciendis laboriosam quasi quaerat nostrum numquam perspiciatis praesentium rem alias.
						</div>
						<div class="mb-2">
							<i class="fas fa-map-marker-alt"></i>
							10 rue Vendrezanne, tour Onyx, Paris 75013
						</div>
						<div class="mb-2">
							<i class="fas fa-subway"></i>
							Métro Place d'Italie
						</div>
						<div class="mb-2">
							<i class="fas fa-phone"></i>
							<span id="phone"></span>
						</div>
						<div class="mb-2">
							<i class="far fa-clock"></i>
							Du lundi au vendredi de 9h30 à 19h
						</div>
					</div>
					<div class="col-md">
						<div class="title">Accès rapide</div>
						<ul>
							@foreach($nav as $link)
								<li><a data-scroll href="{{ $link['action'] }}">{{ $link['title'] }}</a></li>
							@endforeach
								<li><a href="#">Mentions légales</a></li>
						</ul>
					</div>
					<div class="col-md">
						<div class="title">
							Réseaux sociaux
						</div>
						<div>
							Restez informés, suivez nous sur les réseaux sociaux
						</div>
						<div class="socials">
							<a href="#" title="facebook"><i class="fab fa-facebook-square"></i></a>
							<a href="#" title="twitter"><i class="fab fa-twitter-square"></i></a>
							<a href="#" title="linkedin"><i class="fab fa-linkedin"></i></a>
							<a href="#" title="github"><i class="fab fa-github-alt"></i></a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<a class="scroll-top" data-scroll href="#nav" title="Retour en haut"><i class="fas fa-arrow-up"></i></a>
		<script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@16.1.0/dist/smooth-scroll.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.js"></script>
		<script src="/js/app.js"></script>
		@stack('js')
	</body>
</html>
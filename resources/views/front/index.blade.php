@extends('front._layout')

@section('content')
	<section class="full-height pt-5" id="landing">
		<canvas class="background"></canvas>
		<h1 data-aos="fade-up" data-aos-delay="0">BUSINESS & SOLUTIONS</h1>
		<h2 data-aos="fade-up" data-aos-delay="300">Une société sérieuse, dynamique qui vous accompagne dans tous vos projets digitaux</h2>
		<h3 data-aos="fade-up" data-aos-delay="600"> DevOps - Data - Web - Mobile- Blockchain</h3>
		<div>
			<div data-aos="fade-up" data-aos-delay="900">
				<a data-scroll href="#skills" class="btn btn-theme-white">Notre expertise</a>
				<a data-scroll href="#contact" class="btn btn-theme-outline ">Nous contacter</a>
			</div>
		</div>
	</section>

	<section id="skills">
		@php 
			$skills = [
				[
					'title' => 'DevOps',
					'icon' => '<i class="fab fa-docker"></i>',
					'p' => 'Nos experts pourront gérer les infrastructures de vos applications.'
				],
				[
					'title' => 'Data',
					'icon' => '<i class="fas fa-database"></i>',
					'p' => 'Mysql, Postgres, MongoDB...'
				],
				[
					'title' => 'Web',
					'icon' => '<i class="fab fa-php"></i>',
					'p' => 'Du site vitrine aux applications complèxes, nos experts PHP vous accompagneront pour tout type de projet web.'
				],
				[
					'title' => 'Mobile',
					'icon' => '<i class="fas fa-mobile-alt"></i>',
					'p' => 'Nos développeurs Java Kotlin et Swift pourront développer et mettre en ligne vos applications mobiles sur les stores.'
				],
				[
					'title' => 'Blockchain',
					'icon' => '<i class="fas fa-cubes"></i>',
					'p' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi!'
				],
			];
		@endphp

		<h2 class="title" data-aos="fade-up">Exprertises</h2>
		<div class="container-fluid">
			<div class="row">

				@php 
					$delay = 0;
				@endphp

				@foreach($skills as $skill)

					@php 
						$delay += 300;
					@endphp
				

					<div class="col-md" data-aos="fade-up" data-aos-delay="{{ $delay }}">
						<div class="card text-center">
							<div class="card-body">
								{!! $skill['icon'] !!}
								<h3>{{ $skill['title'] }}</h3>
								<div>
									{{ $skill['p'] }}
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section>

	<section class="" id="refs">
		@php
			$refs = [
				'bouygues.svg',
				'natixis.png',
				'sncf.svg',
				'societegenerale.png',
				'keplerk.svg',
				'bpce.png',
				'ovh.png',
				'atos.png',
				'orange.png',
				'manpower.jpeg',
				'staci.png',
				'inops.png',
				'bnp.jpeg',
				'Astorg.gif'
			];
			shuffle($refs);
			
			$refs = array_chunk($refs, 4);

			$delay = 0;
		@endphp

		<h2 class="title h1" data-aos="fade-up">Références</h2>
		<div class="container-fluid">
			@foreach($refs as $ref)
				<div class="refs-row row">
					@foreach($ref as $r)
					
						@php 
							$delay += 300;
						@endphp

						<div class="col-md">
							<div class="ref" data-aos="fade-up" data-aos-delay="{{ $delay }}">
								<img src="/images/logos/{{ $r }}" alt="{{ $r }}" style="max-width: 190px">
							</div>
						</div>
					@endforeach
				</div>
			@endforeach	
		</div>
	</section>

	<section id="join">
		<h3 data-aos="fade-up">Quovadev recrute en permanence de nouveaux talents</h3>
		<a data-scroll href="#contact" data-aos="fade-up" data-aos-delay="300" class="btn">Rejoignez-nous</a>
	</section>

	<section id="offres">
		<h3 class="title" data-aos="fade-up">Dernières offres</h3>
		<div class="row">
			@php 
				$delay = 0;
			@endphp

			@for($i = 0; $i < 3; $i++)
				
				@php 
					$delay += 300;
				@endphp

				<div class="col-md" data-aos="fade-up" data-aos-delay="{{ $delay }}">
					<div class="card">
						<div class="card-body">
							<h5>Title {{ $i }}</h5>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam ea unde id ullam minima consequatur asperiores culpa officia, quae dolorem quisquam officiis facere sequi. Sed quo id, sint consequatur.
							</p>
						</div>
					</div>
				</div>
			@endfor
		</div>
	</section>

	<section id="infos">
		<h3 class="title" data-aos="fade-up">Dernières actualités</h3>

		@php 
			$delay += 300;
		@endphp

		<div class="row">
			@foreach($articles as $article)
				@php 
					$delay += 300;
				@endphp

				<div class="col-md-6" data-aos="fade-up" data-aos-delay="{{ $delay }}">
					<div class="card">
						@if($article->file)
							<div class="card-header">
								<img src="/images/articles/{{ $article->folder }}/{{ $article->file }}" alt="" class="img-fluid">
							</div>
						@endif
						<div class="card-body">
							<h4>{{ $article->title }}</h4>
							<small>{{ $article->created_at }}</small>
							<p>
								{{ $article->preview }}
							</p>
							<div>
								<a href="{{ action('Front\BlogController@getArticle', $article->slug) }}" class="btn btn-primary btn-block">Lire la suite</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>

		<div class="text-center pt-2" data-aos="fade-up" data-aos-delay="{{ $delay + 300 }}">
			<a href="/expertises" class="btn btn-theme">Voir toutes nos actus</a>
		</div>
	</section>

	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col">
					<h4 class="title" data-aos="fade-up">
						Contact
					</h4>
					<form action="{{ action('Front\FrontController@postContact') }}" method="POST" data-aos="fade-up" data-aos-delay="300">
						@csrf
						<div class="row mb-4">
							<div class="col-md">
								<input type="text" class="form-control" placeholder="Nom" maxlength="30" required>
							</div>
							<div class="col-md">
								<input type="text" class="form-control" placeholder="Prénom" maxlength="30" required>
							</div>
							<div class="col-md">
								<input type="email" class="form-control" placeholder="Email" maxlength="200" required>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<textarea name="" class="form-control" rows="10" required></textarea>
							</div>
						</div>
						<button type="submit" class="btn btn-theme btn-block">Envoyer <i class="ml-3 fas fa-paper-plane"></i></button>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('js')

	<script src="https://cdnjs.cloudflare.com/ajax/libs/particlesjs/2.2.3/particles.min.js"></script>
	<script>
		var particles = Particles.init({
			selector: '.background',
			color: ['#FFFFFF', '#404B69'],
			connectParticles: true,
			responsive: [{
				breakpoint: 800,
				options: {
					color: '#00C9B1',
					maxParticles: 80,
					connectParticles: false
				}
			}]
		});
	</script>

	
@endpush
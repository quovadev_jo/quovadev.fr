<div class="card">
	<div class="card-body">
		<div class="h3">Articles au hasard</div>
		@php 
			$articles = App\Models\Article::raw(function($collection){ return $collection->aggregate([ ['$sample' => ['size' => 3]] ]); });
		@endphp
		<ul>
			@foreach($articles as $article)
				<li><a href="{{ action('Front\BlogController@getArticle', $article->slug) }}">{{ $article->title }}</a></li>
			@endforeach
		</ul>
	</div>
</div>
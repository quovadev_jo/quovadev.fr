@extends('front._layout')

@section('content')
	<div class="container-fluid" style="padding-top: 10rem;">
		<div class="row">
			<div class="col-md-8">
				<div class="card mb-5">
					<div class="card-body">
						<article>
							<h1>{{ $article->title }}</h1>
							<p>{{ $article->preview }}</p>
							<p>{{ $article->content }}</p>
							<div>
								<small>{{ $article->created_at }}</small>
							</div>
						</article>
					</div>
				</div>
			</div>

			<div class="col-md">
				@include('front._sidebar')
			</div>
		</div>
	</div>
@endsection
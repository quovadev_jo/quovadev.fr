@extends('front._layout')

@section('content')
	<div class="container-fluid" style="padding-top: 10rem;">
		<div class="row">
			<div class="col-md-8">
				@foreach($articles as $article)
					<div class="card mb-5">
						<div class="card-body">
							<h2><a href="#">{{ $article->title }}</a></h2>
							<div>
								{{ $article->preview }}
							</div>
							<small>{{ $article->created_at }}</small>

							<div>
								<a href="{{ action('Front\BlogController@getArticle', $article->slug) }}" class="btn btn-theme">Lire la suite</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>

			<div class="col-md">
				@include('front._sidebar')
			</div>
		</div>
	</div>
@endsection
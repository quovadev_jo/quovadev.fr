<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Article extends Model
{
	protected $connection = 'mongodb';

	protected $collection = 'articles';

	protected $fillable = [
        'title',
        'slug',
        'preview', 
        'content',
        'folder',
        'file'
    ];
}

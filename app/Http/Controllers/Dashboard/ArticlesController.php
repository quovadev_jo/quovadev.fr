<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Storage;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();

        return view('dashboard.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = action('Dashboard\ArticlesController@store');

        return view('dashboard.articles.create_or_edit', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'preview' => 'required',
            'content' => 'required'
        ]);

        $data = [
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('title')),
            'preview' => $request->get('preview'),
            'content' => $request->get('content'),
            'folder' => str_random(9)
        ];


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            
            $name = $image->getClientOriginalName();
            $name = hash('ripemd128', $name);

            $extension = $image->getClientOriginalExtension();

            $complete_name = "{$name}.{$extension}";

            $image->storeAs("articles/{$data['folder']}", $complete_name);

            $data['file'] = $complete_name;
        }

        Article::create($data);

        return redirect()->action('Dashboard\ArticlesController@index')->with('success', 'Article created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);

        $action = action('Dashboard\ArticlesController@update', ['id' => $article->id]);

        return view('dashboard.articles.create_or_edit', compact('article', 'action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'preview' => 'required',
            'content' => 'required'
        ]);

        $data = [
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('title')),
            'preview' => $request->get('preview'),
            'content' => $request->get('content'),
        ];

        $article = Article::findOrFail($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            
            $name = $image->getClientOriginalName();
            $name = hash('ripemd128', $name);

            $extension = $image->getClientOriginalExtension();

            $complete_name = "{$name}.{$extension}";

            $image->storeAs("articles/{$article->folder}", $complete_name);

            $data['file'] = $complete_name;
        }

        $article->update($data);

        return redirect()->action('Dashboard\ArticlesController@index')->with('success', 'Article updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        
        Storage::deleteDirectory("/articles/{$article->folder}");

        $article->delete();

        return redirect()->back()->with('success', 'Article deleted');
    }
}

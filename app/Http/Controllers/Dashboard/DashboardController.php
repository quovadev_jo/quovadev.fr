<?php

namespace App\Http\Controllers\DashBoard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;


class DashboardController extends Controller
{
    public function getIndex() {
    	return view('dashboard.index');
    }
}

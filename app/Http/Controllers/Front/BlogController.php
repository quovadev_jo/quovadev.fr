<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;


class BlogController extends Controller
{
    public function index() {
    	$articles = Article::paginate(4);

    	return view('front.blog', compact('articles'));
    }

    public function getArticle($slug) {
    	$article = Article::where('slug', $slug)->first();

    	return view('front.article', compact('article'));
    }
}

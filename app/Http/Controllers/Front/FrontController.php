<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;

class FrontController extends Controller
{
    public function getIndex() {
    	$articles = Article::orderBy('id', 'desc')->limit(6)->get();

    	return view('front.index', compact('articles'));
    }

    public function postContact(Request $request) {
    	
    } 
}

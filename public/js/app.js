const nav_ul = document.querySelector('.nav-ul')

document.querySelector('.menu-button').addEventListener('click', (event) => {
	nav_ul.classList.toggle('active')
});


let scroll = new SmoothScroll('a[href*="#"]', {
	speed: 800,
	offset: 100
});

Waves.attach('.btn');
Waves.init();

AOS.init();

//phone 

setTimeout(() => {
	const phone = '01 77 62 80 01';
	document.querySelector('#phone').innerText = phone;
}, 3000);
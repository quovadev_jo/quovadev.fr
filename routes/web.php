<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\FrontController@getIndex');

Route::get('blog', 'Front\BlogController@index');

Route::get('blog/{slug}', 'Front\BlogController@getArticle');

Route::post('contact', 'Front\FrontController@postContact');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'as' => 'dashboard.', 'middleware' => 'auth'], function() {
	Route::get('/', 'DashboardController@getIndex');

	Route::resources([
		'users' => 'UsersController',
		'articles' => 'ArticlesController',
		'jobs' => 'JobsController'
	]);
});

Route::get('logout', function() {
	Auth::logout();
	return redirect('/');
});

Route::get('images/{section}/{folder}/{image}', function($section, $folder, $image) {
	$path = storage_path("app/{$section}/{$folder}/{$image}");
	$image = Image::make($path);
	header('Content-Type: image');
	return $image->response();
});